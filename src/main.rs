use brectools::ecsc::reporting::outage::build_outage_status_report;

const AUTH_PASSWORD: &str = "DZZUPSNRtxn5dNEP";

fn main() {
    let endpoint = "http://10.50.10.185:16499/416/soap/OA_Server";

    match build_outage_status_report(endpoint, "brec", AUTH_PASSWORD) {
        Ok(report) => {
            println!("{}", report);
        }
        Err(_e) => {
            println!("{:?}", _e);
        }
    }
}
